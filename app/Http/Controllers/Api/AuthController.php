<?php
/*================================================================================
    Project: API REST - Banco Capgemini 
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
================================================================================*/ 

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AuthController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:api', ['except' => ['login']]);
        $this->guard = "api";
    }
     /**
     *Pega as credencias do JWT.
     *Get JWT credentials.
     *Create by Carlos Adão 25.08.2020
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(Request $request)
    {
        $credentials = $request->only(['email', 'password']);
        
        if (!$token = auth($this->guard)->attempt($credentials)) {
            return response()->json(['error' => $credentials], 401);
        }
        
        return $this->respondWithToken($token);
    }

    /**
    * Get the token array structure.
    *
    * @param  string $token
    *
    * @return \Illuminate\Http\JsonResponse
    */
    protected function respondWithToken($token)
    {
        return response()->json([
            'access_token' => $token,
            'token_type' => 'bearer',
            'expires_in' => auth($this->guard)->factory()->getTTL() * 60
        ]);
    }
}
