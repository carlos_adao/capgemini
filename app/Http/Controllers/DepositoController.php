<?php
/*================================================================================
    Project: API REST - Banco Capgemini 
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
================================================================================*/ 
namespace App\Http\Controllers;

use App\Auxiliar;
use App\models\Deposito;
use App\Http\Requests\StoreDepositoRequest;
use App\models\Conta;
use Illuminate\Http\Request;


class DepositoController extends Controller
{   
    /**********************************************************/
    /*                          GET                           */
    /**********************************************************/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**********************************************************/
    /*                         POST                           */
    /**********************************************************/
    
    /*Cadastrar um deposito no banco de dados
    *Register a bank account in the database
    *Create by Carlos Adão 25.08.2020*/
    public function store(StoreDepositoRequest $request)
    {   
        //pega os valores de entrada 
        $num_conta  = $request->input('num_conta');
        $valor = Auxiliar::removeMonetary($request->input('valor'));
        $conta = Conta::getContaByNum($num_conta);
        
        //verifica se a conta bancaria existe 
        if(count($conta) == 0){
            return response()->json(['result' => "0"],404);    
        }
        
        //Cria um número de transação 
        $nTrans = Auxiliar::createNumTrans();

        //Instância o objeto deposito
        $dep = new Deposito();
        $dep->num_doc  = $nTrans;
        $dep->valor    = $valor;
        $dep->conta_id = $conta[0]->id;
        $dep->user_id  = $conta[0]->user_id;
        
        if($dep->save()){
            //Incrementa valor saldo
            Conta::depositAmount($conta[0]->id, $valor);
            return response()->json(['result' => "1"],200);
        }

        return response()->json(['result' => "Falha ao realizar transação"],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }
    
    /**********************************************************/
    /*                         PUT                            */
    /**********************************************************/
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /*******************************************************
    *                         DELETE                      * 
    *******************************************************/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
