<?php
/*================================================================================
    Project: API REST - Banco Capgemini 
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
================================================================================*/ 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Auxiliar extends Model
{   
    /*Gera um número de transação de acodo com a data e hora 
    *Generates a transaction number according to the date and time
    *Create by Adão 25.08.2020*/
    public static function createNumTrans(){
        return date('YmdHi') + rand(1, 100);
    }

    /*Gera um número de conta bancaria 
    *Generates a bank account number
    *Create by Adão 25.08.2020*/
    public static function createNumCont(){
        return rand(1000, 9999)."-".rand(1, 9);
    }

    /*Remove caracter monetarios 
    *Remove monetary characters
    *Create by Adão 25.08.2020*/
	public static function removeMonetary($val){ 
		return  str_replace('R$ ', '', str_replace(',00', '', str_replace(',', '.', $val)));
	}


}
