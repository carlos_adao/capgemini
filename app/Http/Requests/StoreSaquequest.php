<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSaquequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *Regras de válidação para requisição saque
     *Validation rules for deposit request
     *Create by Carlos Adão 25.08.2020
     * @return array
     */
    public function rules()
    {
        return [
            'valor'     => 'required',
            'user_id' => 'required',
            'password' => 'required'
        ];
    }

    /*
     *Formatação das mensagens de erro para o português 
     *Formatting error messages in Portuguese
     *Create by Carlos Adão 25.08.2020
     * @return array
     */
    public function messages()
    {
        return[
            'user_id.required'    =>'Usuário não indentificado.',
            'valor.required'      =>'Informe o valor para saque.',
            'password.required'   =>'Informe a senha.',
        ];
    }
}
