<?php

use App\models\Conta;
use Illuminate\Database\Seeder;

class ContaTableSeeder extends Seeder
{
    /**
     *Seeder usado para criar conta bancaria  
     *Seeder used to create  cont bank 
     *Create by Carlos Adão 25.08.2020
     * @return void
     */
    public function run()
    {
        Conta::create([
            'num'     =>"1254-6",
            'amount'  => 0,  
            'tipo'    =>"poupanca",
            'user_id' =>1
        ]);
    }

}
