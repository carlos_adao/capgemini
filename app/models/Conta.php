<?php
/*================================================================================
    Project: API REST - Banco Capgemini 
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
================================================================================*/ 

namespace App\models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Conta extends Model
{
    //Não criar timestamp
    public $timestamps = false;

    /**********************************************************/
    /*                          SELECTS                       */
    /**********************************************************/
    /*Retorna uma conta bancaria de acordo com número da conta
    *Returns a bank account according to the account number
    *create by Adão 25.08.2020*/
    public static function getContaByNum($num_conta)
    {
        return DB::table('contas')
        ->where('num', $num_conta)
        ->get();
    }

    /*Retorna uma conta bancaria do cliente
    *Returns a customer bank account
    *create by Adão 25.08.2020*/
    public static function getContaByUser($user_id, $t_conta)
    {
        return DB::table('contas')
        ->where('user_id', '=', $user_id)
        ->where('tipo', '=', $t_conta)
        ->get();
        
    }

    /**********************************************************/
    /*                          UPDATE                        */
    /**********************************************************/
    /*Realiza o deposito de um valor em uma determinada conta
    *Deposits an amount into a specific account
    *create by Adão 25.08.2020*/
    public static function depositAmount ($id_conta, $valor)
    {
        return DB::table('contas')
        ->where('id', '=', $id_conta)
        ->increment('amount', $valor);
    }

    /*Realiza o Saque de um valor em uma determinada conta
    *Withdraws an amount to a specific account
    *create by Adão 25.08.2020*/
    public static function withdrawsAmount ($id_conta, $valor)
    {
        return DB::table('contas')
        ->where('id', '=', $id_conta)
        ->decrement('amount', $valor);
    }
}
