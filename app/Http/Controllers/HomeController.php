<?php

namespace App\Http\Controllers;


class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /*Abre a tela inicial da aplicação 
    *Opens the application home screen
    *Create by Carlos Adão - jose.carlos.adao@hotmail.com 24.08.2020*/
    public function index()
    {   
        return view('welcome');
    } 
}
