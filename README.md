REPOSITÓRIO

Endereço do repositório no Bitbuket.
https://bitbucket.org/carlos_adao/capgemini/src/master/

COMANDOS INSTALAÇÃO 

Clonar o projeto.
.git clone https://carlos_adao@bitbucket.org/carlos_adao/capgemini.git

Verificar se o endpoint da API está sendo apontado.
acesse seu endereço local da seguinte forma 
Ex Server Laravel:. http://localhost:8080/ 

Executar  Instalação do composer.
composer install

Renomeie o arquivo .env.example para .env
Execulte : composer update.
Para gerar a chave do Laravel execute o comando: php artisan key:generate
O banco de dados utilizado foi o mysql: criar um banco de teste no phpmyadmin: CREATE DATABASE laravel;
Execute php artisan migrate para criar as tabelas no banco de dados.
Execute php artisan db:seed para criar o usuário e a conta padrão para teste. 
Para gerar o JTW execute php artisan jwt:secret
Para testar as requisições utilizei o Postman
A url de consulta será url local + \auth\login
obterá um token de login parecido com esse
{
    "access_token": "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJodHRwOlwvXC90ZXN0LmNhcGdlbWluaS5jb20uYnJcL2FwaVwvYXV0aFwvbG9naW4iLCJpYXQiOjE1OTg0ODY1MDIsImV4cCI6MTU5ODQ5MDEwMiwibmJmIjoxNTk4NDg2NTAyLCJqdGkiOiJjSUhYdGpoUmFDbGp4VUN4Iiwic3ViIjoxLCJwcnYiOiI4N2UwYWYxZWY5ZmQxNTgxMmZkZWM5NzE1M2ExNGUwYjA0NzU0NmFhIn0.5p1W_yZcDlTSFywyvIEHg5BpKOxVkxx7jymtR-a1NPA",
    "token_type": "bearer",
    "expires_in": 3600
}
Coloque o token no Postman e teste as demais rota
Públicas 
login: url local + \auth\login
home:  url local + \
home:  url local + \home
cadastrar depósito: url local + deposito/store
Privadas
cadastrar saque: url local + saque/store
cadastrar conta: url local + conta/store


Materiais e métodos usados

=============================REST API LARAVEL============================
CREATE DATABASE laravel;

//Criação da tabela de contas bancária
php artisan make:migration create_conta_table --create=contas

//Criação da tabela de depósitos
php artisan make:migration create_deposito_table --create=depositos

//Criação da tabela de saques
php artisan make:migration create_saque_table --create=saques

//criação do controller 
php artisan make:controller ContaController --resource
php artisan make:controller DepositoController --resource
php artisan make:controller SaqueController --resource

//criação dos models 
php artisan make:model Conta
php artisan make:model models/Deposito
php artisan make:model models/Saque
php artisan make:model Auxiliar

//criação validação request
php artisan make:request StoreContaRequest 
php artisan make:request StoreDepositoRequest 
php artisan make:request StoreSaquequest 

//Criando uma regra nova 
php artisan make:rule FullName

//Criando um seeder da tabela de usuários 
php artisan make:seed UserTableSeeder

//Criando um seeder da tabela conta bancária
php artisan make:seed ContaTableSeeder


//preencher as tabelas 
php artisan db:seed

===============================JWT======================================


documentação
https://jwt-auth.readthedocs.io/en/develop/laravel-installation/

Publicar configurações jwt 
php artisan vendor:publish --provider="Tymon\JWTAuth\Providers\LaravelServiceProvider"

Gerar chave token 
php artisan jwt:secret
