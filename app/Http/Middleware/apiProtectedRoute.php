<?php
/*================================================================================
    Project: API REST - Banco Capgemini 
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
================================================================================*/ 

namespace App\Http\Middleware;

use Closure;
use Tymon\JWTAuth\Http\Middleware\BaseMiddleware;
use Tymon\JWTAuth\Facades\JWTAuth;

class apiProtectedRoute extends BaseMiddleware
{
    /**
     *Cria o middleware para verificar acesso a api
     *Creates middleware to verify access to api
     *Create by Carlos Adão 25.08.2020*/
    public function handle($request, Closure $next)
    {   
        try{
            $user = JWTAuth::parseToken()->authenticate();
        }catch(\Exception $e){
            if($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                return response()->json(['status' =>'Token invalido']);
            }else if($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                return response()->json(['status' =>'Token expirado']);
            }else{
                return response()->json(['status' =>'Não autorizado']);
            }
        }
        return $next($request);
    }
}
