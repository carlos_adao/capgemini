<?php
/*
|--------------------------------------------------------------------------
|   WEB Routes
|--------------------------------------------------------------------------
| Rotas usadas para implementação front-end laravel blade
| Routes used for laravel blade front-end implementation
*/

Auth::routes();

/*=============================  PUBLIC ROUTES ========================================*/

//Abre a página inicial para checkar endpoint 
Route::get('/', 'HomeController@index')->name('index');

/*============================= PRIVATE ROUTES ========================================*/
Route::middleware(['auth'])->group(function(){
   
});


