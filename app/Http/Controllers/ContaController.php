<?php
/*================================================================================
    Project: API REST - Banco Capgemini 
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
================================================================================*/ 

namespace App\Http\Controllers;

use App\Auxiliar;
use App\Http\Requests\StoreContaRequest;
use App\models\Conta;
use Illuminate\Http\Request;

class ContaController extends Controller
{   

    /**********************************************************/
    /*                          GET                           */
    /**********************************************************/
    /**
    *Retorna uma lista com todas as contas 
    *Returns a list of all accounts
    * @return \Illuminate\Http\Response
    */
    public function index()
    {
        $contas = Conta::get(); 
        return response()->json(['contas' => $contas],200);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**********************************************************/
    /*                         POST                           */
    /**********************************************************/
    
    /*Cadastrar uma conta bancaria no banco de dados
    *Register a bank account in the database
    *Create by Carlos Adão 24.08.2020*/
    public function store(StoreContaRequest $request)
    {   
        $conta = new Conta();
        $conta->num = Auxiliar::createNumCont();
        $conta->amount = 0;
        $conta->tipo = $request->input('tipo');
        $conta->user_id = 1;
        $result  = $conta->save();
        return response()->json(['result' => $result],200);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**********************************************************/
    /*                         PUT                            */
    /**********************************************************/
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /*******************************************************
    *                         DELETE                      * 
    *******************************************************/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
