<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreContaRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *Regras de validação da requisição Criar conta
     *Create account request validation rules
     *Carlos Adão - jose.carlos.adao@hotmail.com 24.08.2020
     * @return array
     */
    public function rules()
    {
        return [
            'cpf'  => 'required',
            'nome' => 'required',
            'tipo' => 'required'
        ];
    }
    public function messages()
    {
        return[
            'cpf.required'  =>'É necessario informar o cpf do cliente',
            'nome.required'  =>'É necessario informar o nome do cliente',
            'tipo.required'     =>'O tipo de conta é obrigatorio.'
        ];
    }
}
