<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreDepositoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     *Regras de válidação para requisição deposito
     *Validation rules for deposit request
     *Create by Carlos Adão 25.08.2020
     * @return array
     */
    public function rules()
    {
        return [
            'valor'     => 'required',
            'num_conta' => 'required'
        ];
    }

    /*
     *Formatação das mensagens de erro para o português 
     *Formatting error messages in Portuguese
     *Create by Carlos Adão 25.08.2020
     * @return array
     */
    public function messages()
    {
        return[
            'num.valor'      =>'O Número da conta é obrigatorio.',
            'conta_id.required'   =>'O saldo inicial é obrigatorio.',
        ];
    }
}
