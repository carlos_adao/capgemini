<?php
/*================================================================================
    Project: API REST - Banco Capgemini 
    Version: 1.0
    Author: Carlos Adão
    Mail: jose.carlos.adao@hotmail.com
================================================================================*/ 

namespace App\Http\Controllers;

use App\Auxiliar;
use App\Http\Requests\StoreSaquequest;
use App\models\Conta;
use App\Saque;
use Illuminate\Http\Request;

class SaqueController extends Controller
{   
    /**********************************************************/
    /*                          GET                           */
    /**********************************************************/
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**********************************************************/
    /*                         POST                           */
    /**********************************************************/
    
    /*Cadastrar um saque em uma conta bancaria
    *Register a withdrawal to a bank account
    *Create by Carlos Adão 24.08.2020*/
    public function store(StoreSaquequest $request)
    {
        //pega os valores de entrada 
        $user_id    = $request->input('user_id');
        $valor      = Auxiliar::removeMonetary($request->input('valor'));
        $t_conta    = $request->input('t_conta');
        $password   = $request->input('password');
        $conta = Conta::getContaByUser($user_id, $t_conta);
        
        //verifica se a conta bancaria existe 
        if(count($conta) == 0){
            return response()->json(['result' => "0"],404);    
        }
        
        //verificar se o saldo em conta é maior que o saque 
        if($conta[0]->amount < $valor){
            return response()->json(['result' => "Saldo insuficiente"],404);    
        }

        //Cria um número de transação 
        $nDoc = Auxiliar::createNumTrans();

        //Instância o objeto saque
        $saq = new Saque();
        $saq->num_doc  = $nDoc;
        $saq->valor    = $valor;
        $saq->conta_id = $conta[0]->id;
        $saq->user_id  = $conta[0]->user_id;
        
        if($saq->save()){
            //Incrementa valor saldo
            Conta::withdrawsAmount($conta[0]->id, $valor);
            return response()->json(['result' => $conta[0]->amount],200);
        }

        return response()->json(['result' => "3"],500);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**********************************************************/
    /*                         PUT                            */
    /**********************************************************/
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /*******************************************************
    *                         DELETE                      * 
    *******************************************************/
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
