/*Função usda para caracter dos valores monetarios
Created by Carlos Adão - 15.05.2020*/
removeCaracterMoney = function(money) {
    money = money.replace("R$ ", "");
    money = money.replace("R$", "");
    money = money.replace(" ", "");
    money = money.replace(/\s/g, "");
    money = money.replace(".", "");
    money = money.replace(",", ".");
    return money;
}