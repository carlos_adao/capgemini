<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDepositoTable extends Migration
{
    /**
     *Cria a tabela de depositos
     *Create the bank account table*/
    public function up()
    {
        Schema::create('depositos', function (Blueprint $table) {
            $table->id();
            $table->string('num_doc',20);
            $table->float('valor', 8, 2);
            $table->timestamps();
            $table->string('tel_contato',20);
            $table->unsignedBigInteger('conta_id');
            $table->unsignedBigInteger('user_id');
            $table->foreign('conta_id')->references('id')->on('contas')->onDelete('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('depositos');
    }
}
