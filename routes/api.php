<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
| Rotas usada para implementação dos métodos de evento e ação da API Capgemini Test
| Routes used to implement Capgemini Test API action and event methods
*/

/*=============================  PUBLIC ROUTES ========================================*/
//Realiza um deposito 
Route::post('deposito/store', 'DepositoController@store')->name('deposito.store');

//Realizar o login 
Route::post('auth/login', 'Api\AuthController@login');

/*============================= PRIVATE ROUTES ========================================*/

Route::group(['middleware'=>['apiJwt']], function(){
    
    //Criar uma nova conta
    Route::get('contas', 'ContaController@index')->name('conta.list');
    
    //Realiza um saque 
    Route::post('saque/store', 'SaqueController@store')->name('saque.store');
});


