<?php

use App\User;
use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
   /**
     *Seeder usado para criar user   
     *Seeder used to create  cont bank 
     *Create by Carlos Adão 25.08.2020
     * @return void
     */
    public function run()
    {
        User::create([
            'name'     =>"admim",
            'cpf'      =>"00000000000",  
            'email'    =>"admim@apicapgemini.com.br",
            'password' =>bcrypt("1234")
        ]);
    }
}
